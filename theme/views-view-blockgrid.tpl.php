<?php

/**
 * @file
 * A custom format for zurb block grid.
 *
 * @ingroup views_templates
 */

/**
 * Zurb docs: http://foundation.zurb.com/docs/components/block_grid.html.
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul<?php print $attributes; ?>>
  <?php foreach ($rows as $id => $row): ?>
    <li<?php print $row_attributes[$id]; ?>>
      <div class="block-content">
        <?php print $row; ?>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
