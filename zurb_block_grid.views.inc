<?php

/**
 * @file
 * Uses ctool plugins to add custom format to views.
 */

/**
 * Implements hook_views_plugins().
 */
function zurb_block_grid_views_plugins() {
  return array(
    'style' => array(
      'zurbgrid' => array(
        'title' => t('Zurb Block Grid'),
        'help' => t('Display the results in a zurb block widget.'),
        'handler' => 'zurb_block_grid_plugin_style_zurbgrid',
        'theme' => 'views_view_blockgrid',
        'register theme' => FALSE,
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses fields' => FALSE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
