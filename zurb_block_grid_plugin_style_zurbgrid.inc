<?php
/**
 * @file
 * Zurb Block Grid style plugin for the Views module.
 */

/**
 * Implements a style type plugin for the Views module.
 *
 * @ingroup views_style_plugins
 */
class zurb_block_grid_plugin_style_zurbgrid extends views_plugin_style {

  /**
   * Implements parent::options_definition().
   *
   * Define option defaults.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['grid_settings'] = array(
      'contains' => array(
        'small' => array(
          'default' => 1,
        ),
        'medium' => array(
          'default' => 2,
        ),
        'large' => array(
          'default' => 3,
        ),
      ),
    );
    $options['custom_class'] = array(
      'default' => '',
    );
    return $options;
  }

  /**
   * Implements parent::options_form().
   *
   * Build the format configuration form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['grid_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Zurb Block Grid Configuration'),
      '#value' => 'Set the small, medium and large configuration for zurb block grid.',
    );

    $form['grid_settings']['small'] = array(
      '#type' => 'textfield',
      '#title' => t('Small'),
      '#default_value' => $this->options['grid_settings']['small'],
      '#size' => 60,
      '#maxlength' => 2,
    );

    $form['grid_settings']['medium'] = array(
      '#type' => 'textfield',
      '#title' => t('Medium'),
      '#default_value' => $this->options['grid_settings']['medium'],
      '#size' => 60,
      '#maxlength' => 2,
    );

    $form['grid_settings']['large'] = array(
      '#type' => 'textfield',
      '#title' => t('Large'),
      '#default_value' => $this->options['grid_settings']['large'],
      '#size' => 60,
      '#maxlength' => 2,
    );

    $form['custom_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom Class'),
      '#default_value' => $this->options['custom_class'],
      '#size' => 60,
      '#maxlength' => 128,
    );
  }

}
