CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
The Zurb Block Grid module will allow you to set a format in views which will
utilize zurb block grid.

* For a full description of the module, visit the project page:
  https://drupal.org/project/zurb_block_grid


* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/zurb_block_grid


REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)
 * Ctools (https://drupal.org/project/ctools)
 * Zurb Foundation (https://drupal.org/project/zurb_foundation)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * After creating a view you can set the format to Zurb Block Grid. This format
   has settings for large, medium and small as well as some standard view
   options.


MAINTAINERS
-----------
Current maintainers:
 * Joseph Leon - https://www.drupal.org/user/1891828


This project has been sponsored by:
 * Last Call Media
   Drupal web developers near the Amherst, Northampton and Western Massachusetts
   area, specializing in ongoing Drupal support and deployments.
   Visit https://lastcallmedia.com/ for more information.
